# User Story:

`System-Implementation/Component` and `Control-Implementation/implemented-requirements` can both utilize `links` to create logical and technical relationships between components and the controls they work with.  Currently the `Link` assembly provides for an `href` and `rel` field.  The `rel` field currently allows any token, but only provides `reference` as a suggested or directly `allowed-value` tokens.  

Other meta-schema assemblies of Link provide more common relation notation

During a discussion the following pseudocode represents a possible use of these tokens

```
control-implementation
- implemented-requirements
  - control[@id="enc-1"]
    - by-component[@component-uuid="#storage-array-uuid"]
      - description: implementation narrative
    - by-component[@component-uuid="app1"]
      - link[@rel="provided-by", @href="#storage-array-uuid"]
      - description: encryption is provided by the storage array
```


## Goals:

At minimal, I would like to add `provided-by` or `satisfied-by` as a possible and valid relation to links defined under the `by-component` stanza.  This will allow all components to be listed under a control and be able to specific which other components assist in the satisfaction of the control.


## Dependencies:

N/A

## Acceptance Criteria

- [ ] All [OSCAL website](https://pages.nist.gov/OSCAL) and readme documentation affected by the changes in this issue have been updated. Changes to the OSCAL website can be made in the docs/content directory of your branch.
- [ ] A Pull Request (PR) is submitted that fully addresses the goals of this User Story. This issue is referenced in the PR.
- [ ] The CI-CD build process runs without any reported errors on the PR. This can be confirmed by reviewing that all checks have passed in the PR.

{The items above are general acceptance criteria for all User Stories. Please describe anything else that must be completed for this issue to be considered resolved.}
