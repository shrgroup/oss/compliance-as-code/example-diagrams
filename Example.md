# Example System

To demonstrate this example scenario we will use a small fictitious example system.  It consists of a storage array, an application server, and an authentication server.

```uml
person "Users" as users

component "Application Server" as app 
component "Storage Array" as store
component "Authentication" as auth

users -- app 
app <--> store
app <-> auth
```

We will also create/base the documents on a fictitious catalog of controls.  The controls are inspired by 800.53, but to keep the models small and simple, do NOT map to it, nor have the same details associated.  The Catalog only contains 4 basic controls 

* **ENC-1:** states the system must utilize at-rest encryption
* **AUTH-1:** System must support password complexity requirements of length greater than <n> characters
* **AVAIL-1:** Provides site redundancy
* **SUP-1:** Utilizes multiple vendors

## The Catalog

To create a catalog that will be used by the SSP, we will take the source Catalog, and apply a profile to filter, modify, and update the source catalog.  This will create a new `Catalog` either on-demand, or persistently.

```uml
!include 

## The Components


## The SSP

