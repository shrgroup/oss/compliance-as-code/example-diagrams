# User Story:

To better document how components, capabilities, and controls all relate to one another.  Additional pre-defined relation types would be useful.  Although the Link assembly allows for arbitrary values some of the time, pre-defined acceptable values would provide users and tool developers better ability to make more universally compatible documents.

Adding additional values to the following Links would simplify importing and referencing source information for objects within the SSP

## Goals:

Add `imported-from` as a accepted value in the ssp -> system implementation -> component -> link stanza.  This relation is intended to represent the UUID and/or href back to the Component Definition that defined that component.

Add `imported-from` as a accepted value in the ssp -> Control implementation -> implemented requirements -> link stanza.  This relation is intended to represent the UUID and/or href back to the Component Definition that defined that control narrative.

## Dependencies:

N/A 

## Acceptance Criteria

- [ ] All [OSCAL website](https://pages.nist.gov/OSCAL) and readme documentation affected by the changes in this issue have been updated. Changes to the OSCAL website can be made in the docs/content directory of your branch.
- [ ] A Pull Request (PR) is submitted that fully addresses the goals of this User Story. This issue is referenced in the PR.
- [ ] The CI-CD build process runs without any reported errors on the PR. This can be confirmed by reviewing that all checks have passed in the PR.

{The items above are general acceptance criteria for all User Stories. Please describe anything else that must be completed for this issue to be considered resolved.}
