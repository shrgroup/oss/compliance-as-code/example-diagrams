# Component Relations and Inheritence

## Scope

This document will focus solely on components, where the details are documented, and how relations are expressed in OSCAL.  All models discussed have vast amounts of additional mandatory and optional stanzas and data that are left out for brevity.

It is the hope that this scenario, and examples, can be combined with additional other brief subsets to provide enough knowledge to generate complete and compliant OSCAL documents.

## System


To demonstrate this example scenario we will use a small fictitious example system.  It consists of a storage array, an application server, and an authentication server.

![app](imgs/app.png)

We will also create/base the documents on a fictitious catalog of controls.  The controls are inspired by 800.53, but to keep the models small and simple, do NOT map to it, nor have the same details associated.  The Catalog only contains 3 basic controls 

* **ENC-1:** states the system must utilize at-rest encryption
* **AUTH-1:** System must support password complexity requirements of length greater than <n> characters
* **SUP-1:** Utilizes multiple vendors

## The Catalog

To create a catalog that will be used by the SSP, we will take the source Catalog, and apply a profile to filter, modify, and update the source catalog.  This will create a new `Catalog` either on-demand, or persistently.

![profile_resolve](imgs/profile_resolve.png)

## The Components

In this example, every component we include will be defined in its own `Component-Definition`.  This is not a requirement of OSCAL, but is done to allow each component to be more easily included in multiple projects.  It also allows the engineers that are responsible for those specific components, to also author the Compliance information without needing to know about the system as a whole.

So this phase we introduce 3 new *documents* using the Component Definition Model.  The Component Definition (CDEF) contains many of the common elements, Metadata, Links, etc.  But for this scenario we can focus on components, and capabilities.  

The first will introduce the details of the Storage Array.  This would include the vendor, software, versions, and implementation details common to ANY system using this array.  Our Mock or Pretend storage array has 3 components, and one capability.

Components include the Software or OS running, the Hardware Version of the core unit, and details about the disk shelf.  Remember, real system would have FAR more, but we are minimizing it for better discussing the concept.  The Capability in this case would be its main function *Network Attached Storage*.

![storage_cdef](imgs/storage_cdef.png)

Next we have a CDEF for the authentication *service*.  In this example, the CDEF will represent not only the software deployed, but the Infrastructure as Code used to Deploy it.  The CDEF would consist of 2 components: the IaC (such as ansible, Helm, or TF), and the binary of the service (such as an AMI, Docker Container, or application executable).  The Capability documented is *Authentication*.

![auth_cdef](imgs/auth_cdef.png)


The final Definition is for the main application.  Like the authentication server, this will consist of 2 components: the IaC for deploying it, and the binary or executable for the application.

![app_cdef](imgs/app_cdef.png)

## The SSP

The SSP represents the system as a whole, not just any one component.  As such it will incorporate all the Definitions we just discussed, as well as capture the relationships between them.  It will also document how those components together provide a compliant system.

The SSP imports any reference documents to ensure the UUIDs are addressiable, and information does not need to be duplicated.

![ssp_import](imgs/ssp_import.png)

As always, there is a large of amount of information in the SSP that isn't discussed in this document, including roles, policies, metadata, and additional system information.  This scenario is *currently* focused on documenting components and component relations.

![ssp](imgs/ssp.png)

For every control met, we must show that every component either meets the control, or is exempt from it.  Therefore every control has a `by-component` stanza for every component.  If the component meets the control because of a dependency, as is shown with the at-rest encryption example, it uses a link with the relation `provided-by` to reference the component that ensures the control is met.  If the control isn't relevant, and therefor the component is exempt, a property is used to mark it as `Not-Applicable`.  It is recommended that a description is included to document why that control is exempt.


